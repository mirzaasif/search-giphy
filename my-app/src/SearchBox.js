import 'isomorphic-fetch';
import React from 'react';
import {Container, TextField, Button, Input} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';


export default function SearchBox(props) {
  const [options, setOptions] = React.useState([]);
  const [input, setInput] = React.useState('');
  const search = async (value) => {
    if (value !== null && value !== ''){
        const response = await fetch('http://127.0.0.1:5000/search?s='+value);
        const data = await response.json();
        props.onResults(data.results);
    }
  };

  return (
    <Container>
        <Autocomplete
        id="asynchronous-demo"
        options={options}
        freeSolo={true}
        filterOptions={(options) => {
            return options;
        }}
        onChange={async (event, value) => {
            await search(value);
        }}
        onInputChange={async (event, value) => {
            if (value !== null && value !== ''){
            const response = await fetch('http://127.0.0.1:5000/search-suggestion?s='+value);
            const data = await response.json();
                setOptions(data.results);
            }else {
                setOptions([]);
            }
            setInput(value);
        }}
        renderInput={params => (
            <TextField
            {...params}
            label="Search"
            fullWidth
            variant="outlined"
            />
        )}
        />
        <div style={{paddingTop: '20px'}}>
            <Button variant="contained" color="primary" onClick={async () => {await search(input); }}>Go</Button>
        </div>
    </Container>
  );
}