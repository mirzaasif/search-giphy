import React from 'react';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Link } from '@material-ui/core';
import SearchBox from './SearchBox.js'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
    flexWrap: 'wrap',
    paddingTop: '40px',
    paddingLeft: '80px',
  },
  img: {
    width: theme.spacing(12),
    height: theme.spacing(12),
  },
  img: {
    width: theme.spacing(12),
    height: theme.spacing(12),
  },
}));

function App() {
  const classes = useStyles();
  const [results, setResults] = React.useState([]);
  return (
    <div className="App">
        <Container className="Container">
          <h3 className="Title">Rhumbix Programming Test</h3>
          <SearchBox onResults={setResults}></SearchBox>
          <div className={classes.root}>
            {results.map(result => (
              <Link href={result.url} target="_blank">
                <img src={result.img} alt="" key={result.id}/>
              </Link>
            ))}
          </div>
        </Container>
    </div>
  );
}

export default App;
