from pytrie import SortedStringTrie
from flask import Flask, jsonify, request
from flask_cors import CORS
import requests

app = Flask(__name__)
CORS(app)
trie = SortedStringTrie()

INITIAL_DATA = [
    'about',
    'above',
    'across',
    'app',
    'apple',
    'appreciate',
    'bad',
    'ball',
    'balloon',
    'bell',
    'cat',
]


def load_initial_data():
    for word in INITIAL_DATA:
        trie[word] = 1


@app.route('/search', methods=['GET'])
def search():
    keyword = request.args.get('s', None)
    if keyword is None:
        return jsonify({'results': []})    

    keyword = keyword.lower()
    if keyword not in trie:
        trie[keyword] = 1
    else:
        trie[keyword] += 1

    response = requests.get('http://api.giphy.com/v1/gifs/search?q={}&api_key=DLCVuTK6KZExOS7JoMq82bi5MaI6EbWO&limit=20'.format(keyword))
    data = response.json()

    results = []
    for img in data['data']:
        results.append({
            'id': img['id'],
            'url': img['url'],
            'img': img['images']['fixed_width']['url'],
        })
    
    return jsonify({'results': results})


@app.route('/search-suggestion', methods=['GET'])
def search_suggestion():
    keyword = request.args.get('s')
    values = trie.keys(prefix=keyword)
    return jsonify({'results': values})


if __name__ == '__main__':
    load_initial_data()
    app.run(debug=True)